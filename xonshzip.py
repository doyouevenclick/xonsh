import site
import sys
import os.path
from xontrib.voxapi import Vox


aliases['xpip'] = os.path.join(site.USER_BASE, 'bin', 'pip')

# Do this at the end, so system stuff masks anything
# Namely pip, python, etc
__xonsh__.env['PATH'].append(os.path.join(site.USER_BASE, 'bin'))


def _setup():
    vox = Vox()
    # Use the original executable, not any potential venv one
    vox.create(site.USER_BASE, interpreter=sys.__executable__)
    site.addusersitepackages(None)

    # Find the right path, add a xontrib dir
    # This is to make sure that the xontrib namespace package works before any
    # xontribs are installed
    for vpath in sys.path:
        if vpath.startswith(site.USER_BASE):
            break
    else:
        vpath = None
    if vpath is not None:
        xontribpath = os.path.join(vpath, 'xontrib')
        os.makedirs(xontribpath, exist_ok=True)
        for xname in ('prompt_ret_code.xsh', 'jedi.xsh'):
            with open(os.path.join(xontribpath, xname), 'wb') as dest:
                dest.write(__loader__.get_data('xontrib/' + xname))


    # TODO: Install wheel, to improve running xpip

    print("Please reload xonsh")

aliases['home-environ-setup'] = _setup


def _upgrade():
    vox = Vox()
    # Use the original executable, not any potential venv one
    vox.upgrade(site.USER_BASE, interpreter=sys.__executable__)
    print("Please reload xonsh")

aliases['home-environ-upgrade'] = _upgrade

#!/usr/bin/env python3
import sys
import os.path
import subprocess
import shutil
import tempfile
import zipapp
from pathlib import Path


def build(cmd, packages):
    with tempfile.TemporaryDirectory() as tmpdir:
        subprocess.run(
            [sys.executable, '-m', 'venv', tmpdir],
            check=True
        )
        interp = os.path.join(tmpdir, 'bin', 'python')  # FIXME: Linux only

        # This makes further installation quieter
        subprocess.run(
            [interp, '-m', 'pip', 'install', 'wheel'],
            check=True
        )

        subprocess.run(
            [interp, '-m', 'pip', 'install', *packages],
            check=True
        )

        libdir = os.path.join(tmpdir, 'lib', "python%d.%d" % sys.version_info[:2], 'site-packages')
        shutil.copyfile(
            os.path.join('main.py'),
            os.path.join(libdir, '__main__.py')
        )
        shutil.copyfile(
            os.path.join('xonshzip.py'),
            os.path.join(libdir, 'xontrib', 'xonshzip.py')
        )

        # 3.7: Use filter in create_archive() instead
        for p in Path(libdir).glob('**/__amalgam__.py'):
            p.unlink()

        zipapp.create_archive(
            source=libdir,
            target=cmd,
            interpreter="{} -IOBu".format(sys.executable))


if __name__ == '__main__':
    build("xonsh", [
        "xonsh[linux,ptk,pygments,zipapp] >= 0.9.7",
        'importlib_resources; python_version < "3.7"',
        # "xontrib-avox",
        # "xontrib-z",
        # "xontrib-schedule",
        # "https://github.com/AstraLuma/xontrib-salt/archive/master.zip",
    ])

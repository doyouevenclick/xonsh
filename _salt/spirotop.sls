stretch:
  "G@os:Debian and G@oscodename:stretch":
     - match: compound
     - xonsh

buster:
  "G@os:Debian and G@oscodename:buster":
     - match: compound
     - xonsh

bullseye:
  "G@os:Debian and G@oscodename:bullseye":
     - match: compound
     - xonsh

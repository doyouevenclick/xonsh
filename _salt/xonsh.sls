# Python is implied by platform
#python3: pkg.installed  # Fedora, debian

/usr/local/bin/xonsh:
  file.managed:
    - source: salt://_artifacts/xonsh
    - mode: 0755

/etc/shells:
  file.append:
    - text: /usr/local/bin/xonsh
    - require:
      - file: /usr/local/bin/xonsh

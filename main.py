import sys
import os.path

# Make sure the paths are resolved
sys.path = [os.path.abspath(component) for component in sys.path]


# Set up a xonsh-specific site directory
import site


def _isolated_checkuser():
    # Blatently stolen from site.check_enableusersite()
    if hasattr(os, "getuid") and hasattr(os, "geteuid"):
        # check process uid == effective uid
        if os.geteuid() != os.getuid():
            return None
    if hasattr(os, "getgid") and hasattr(os, "getegid"):
        # check process gid == effective gid
        if os.getegid() != os.getgid():
            return None
    return True


site.USER_BASE = os.path.join(site.getuserbase(), 'xonsh')
site.USER_SITE = None
site.ENABLE_USER_SITE = _isolated_checkuser()
site.addusersitepackages(None)


# Hook sys.executable, so anything downstream runs in the venv
sys.__executable__ = sys.executable
if os.path.isdir(site.USER_SITE):
    vexec = os.path.join(site.USER_SITE, 'bin', 'python')  # FIXME: Linux only
    if os.path.exists(vexec):
        sys.executable = vexec


# Actually run xonsh
from xonsh.main import main
main()
